package teht14;

public class McDonaldsBuilder extends Builder{

    private McDonaldsBurger burger;
    @Override
    public void createNewBurger() {
        burger = new McDonaldsBurger();
    }

    @Override
    public void buildPihvi() {
        burger.addAines("pihvi");
    }

    @Override
    public void buildJuusto() {
        burger.addAines("juusto");
    }

    @Override
    public void buildSalaatti() {
        burger.addAines("salaatti");
    }

    @Override
    public void buildSämpylä() {
        burger.addAines("sämpylä");
    }

    @Override
    public Object getBurger() {
        return burger.getBurger();
    }
}
