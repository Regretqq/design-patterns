package teht14;

public abstract class Builder {

    public abstract void createNewBurger();
    public abstract void buildPihvi();
    public abstract void buildJuusto();
    public abstract void buildSalaatti();
    public abstract void buildSämpylä();

    public abstract Object getBurger();

}
