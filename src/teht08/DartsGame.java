package teht08;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DartsGame extends Game{

    private List<DartsPlayer> players;
    private Scanner sc = new Scanner(System.in);
    private boolean end = false;
    private int winner;


    @Override
    void initializeGame() {
        players = new ArrayList<>();
        for(int i = 0; i < playersCount; i++){
            players.add(new DartsPlayer());
        }
    }

    @Override
    void makePlay(int player) {
        System.out.println("Pelaajan " + player + " vuoro");
        DartsPlayer currentPlayer = players.get(player);
        int startScore = currentPlayer.getCurrentScore();
        System.out.println("Pelaajan " + player + " pisteet: " + startScore);
        for(int i = 0; i < 3; i++){

            System.out.println("Anna heiton tulos:");
            int score = Integer.parseInt(sc.nextLine());
            currentPlayer.addThrow(score);
            if(currentPlayer.getCurrentScore() == 0){
                end = true;
                winner = player;
                return;
            }else if(currentPlayer.getCurrentScore() < 0){
                currentPlayer.setCurrentScore(startScore);
                System.out.println("Pelaajan " + player + " pisteet meni alle nollan");
                System.out.println("Pelaajan " + player + " pisteet: " + currentPlayer.getCurrentScore());
                return;
            }
            System.out.println("Pelaajan " + player + " pisteet: " + currentPlayer.getCurrentScore());
        }
    }

    @Override
    boolean endOfGame() {
        return end;
    }

    @Override
    void printWinner() {
        System.out.println("Pelaaja " + winner + " on voittaja!");
    }


}
