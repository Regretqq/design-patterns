package teht08;

public class DartsPlayer {

    private int currentScore;

    public DartsPlayer(){
        currentScore = 301;
    }

    public void addThrow(int score){
        currentScore -= score;

    }

    public int getCurrentScore(){
        return currentScore;
    }

    public void setCurrentScore(int currentScore){
        this.currentScore = currentScore;
    }
}
