package teht11;

import teht04.Observer;

import java.util.Random;

public class Arvaaja extends Thread{

    private Object vastaus;
    private Arvuuttaja arvuuttaja;
    private Random random;
    private static int i = 1;
    private String nimi;

    public Arvaaja(Arvuuttaja arvuuttaja){
        this.arvuuttaja = arvuuttaja;
        vastaus = arvuuttaja.liityPeliin();
        random = new Random();
        nimi = "Pelaaja" + i++;

    }

    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int arvaus = random.nextInt(11);
            if(arvuuttaja.tarkistaArvaus(vastaus, arvaus)){
                System.out.println(nimi + " arvasi oikein (" + arvaus + ")");
                return;
            }
            System.out.println(nimi + " arvasi väärin (" + arvaus + ")");

        }
    }
}
