package teht12;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Image> kansio = new ArrayList<>();

        for(int i = 0; i < 10; i++){
            kansio.add(new ProxyImage("kuva" + (i+1)));
        }

        for(Image a : kansio){
            System.out.println(a.showData());
        }

        for(Image a : kansio){
            a.displayImage();
        }
    }
}
