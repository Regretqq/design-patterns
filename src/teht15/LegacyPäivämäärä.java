package teht15;

public class LegacyPäivämäärä {

    private int päivä = 13;
    private int kuukausi = 9;
    private int vuosi = 2020;


    public int getKuukausi() {
        return kuukausi;
    }

    public int getPäivä() {
        return päivä;
    }

    public int getVuosi() {
        return vuosi;
    }
}
