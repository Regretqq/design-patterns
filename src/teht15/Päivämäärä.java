package teht15;

import java.time.LocalDate;

public class Päivämäärä extends LegacyPäivämäärä implements aPäivämäärä{


    @Override
    public LocalDate getPäivämäärä() {
        LegacyPäivämäärä pvm  = new LegacyPäivämäärä();
        return LocalDate.of(pvm.getVuosi(), pvm.getKuukausi(), pvm.getPäivä());
    }
}
