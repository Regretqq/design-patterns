package teht06;

public abstract class Decorator implements Window{

    protected Window decoratedData;

    public Decorator(Window decoratedData){
        this.decoratedData = decoratedData;
    }

    public Data getInfo(){
        return decoratedData.getInfo();
    }


}
