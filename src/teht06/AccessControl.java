package teht06;

import java.util.Scanner;

public class AccessControl extends Decorator{

    public AccessControl(Window decoratedData) {
        super(decoratedData);
    }

    public Data getInfo(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Anna salasana: ");
        String syöte = sc.nextLine();
        if(syöte.equals("testi")){
            return super.getInfo();
        }else{
            return null;
        }
    }
}
