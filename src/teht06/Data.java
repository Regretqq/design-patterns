package teht06;

public class Data implements Window {


    private String tuote = "tietokone";
    private int hinta = 1500;

    @Override
    public Data getInfo() {
        return this;
    }

    public String toString(){
        return "tuote on " + tuote + " ja sen hinta on " + hinta + " euroa.";
    }


}
