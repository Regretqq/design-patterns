package teht03;

public interface Grafiikkakortti extends Komponentti{

    void lisääKomponentti(Komponentti komponentti);

    int getHinta();
}
