package teht10;

public class LähiEsimies extends Palkankorotus{

    public LähiEsimies(Palkankorotus esimies){
        super(esimies);
    }

    @Override
    public boolean käsittelePalkankorotus(double korotus) {
        if(korotus <= 2){
            System.out.println("Lähiesimies hyväksyi palkankorouksen");
            return true;
        }else {
            System.out.println("Lähiesimies lähetti palkankorotuksen Päälikkölle");
            return super.käsittelePalkankorotus(korotus);
        }
    }
}
