package teht13;

public class Pokemon {

    private aCharmander p;
    private int currentXP = 0;
    private boolean evolved = false;
    private Visitor visitor;

    public Pokemon(){

        p = new Charmander();
        visitor = new Visitor();
        }

    public void attack() {
        p.attack();
    }

    public void giveXP(int xp){
        currentXP += xp;
        visitor.visit(this);
    }

    public String getStateName(){
        return p.getState();
    }


    public int getCurrentXP(){
        return currentXP;
    }

    public void setCurrentXP(int xp){
        currentXP = xp;
    }

    public aCharmander getState(){
        return p;
    }

    public void setState(aCharmander p){
        this.p = p;
    }

}
