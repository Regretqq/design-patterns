package teht13;

public class Visitor {

    private boolean evolved = false;

    public void visit(Pokemon pokemon){
        do {
            if (pokemon.getState().checkEvolve(pokemon.getCurrentXP())) {
                pokemon.setCurrentXP(pokemon.getCurrentXP() - pokemon.getState().getXpToEvolve());
                pokemon.setState(pokemon.getState().evolve());
                evolved = true;
                System.out.println("Pokemon kehittyi " + pokemon.getState().getState() + "ksi");
            }else{
                evolved = false;
            }
        }while (evolved);
    }
}
