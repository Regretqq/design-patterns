package teht13;

public abstract class aCharmander {



    abstract void attack();

    abstract String getState();

    abstract boolean checkEvolve(int xp);

    abstract aCharmander evolve();

    abstract int getXpToEvolve();


}
