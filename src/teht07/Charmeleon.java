package teht07;

public class Charmeleon extends aCharmander {
   private final int xpToEvolve = 300;

    @Override
    void attack() {
        System.out.println("Charmeleon hyökkäsi");
    }

    @Override
    String getState() {
        return "Charmeleon";
    }

    @Override
    boolean checkEvolve(int xp) {
        if(xp > xpToEvolve){
            return true;
        }
        return false;
    }

    @Override
    aCharmander evolve() {
        return new Charizard();
    }

    @Override
    int getXpToEvolve() {
        return xpToEvolve;
    }


}
