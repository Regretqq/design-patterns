package teht07;

public class Charmander extends aCharmander{


    private final int xpToEvolve = 100;


    @Override
    void attack() {
        System.out.println("charmander hyökkäsi");
    }

    @Override
    String getState() {
        return "Charmander";
    }


    @Override
    boolean checkEvolve(int xp) {
        if(xp > xpToEvolve){
            return true;
        }
        return false;
    }

    @Override
    aCharmander evolve() {
        return new Charmeleon();
    }

    @Override
    int getXpToEvolve() {
        return xpToEvolve;
    }


}
