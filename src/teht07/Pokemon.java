package teht07;

public class Pokemon {

    private aCharmander p;
    private int currentXP = 0;
    private boolean evolved = false;

    public Pokemon(){
            p = new Charmander();
        }

    public void attack() {
        p.attack();
    }

    public void giveXP(int xp){
        currentXP += xp;
        checkEvolve();
    }

    public String getState(){
        return p.getState();
    }

    public void checkEvolve(){
        do {
            if (p.checkEvolve(currentXP)) {
                currentXP = currentXP - p.getXpToEvolve();
                p = p.evolve();
                evolved = true;
                System.out.println("Pokemon kehittyi " + p.getState() + "ksi");
            }else{
                evolved = false;
            }
        }while (evolved);
    }

}
