package teht07;

public class Main {

    public static void main(String[] args) {

        Pokemon pokemon = new Pokemon();

        System.out.println("Pokemon on " + pokemon.getState());
        pokemon.attack();
        pokemon.giveXP(125);
        pokemon.attack();
        pokemon.giveXP(475);
        pokemon.attack();

    }
}
