package teht07;

public class Charizard extends aCharmander{


    @Override
    void attack() {
        System.out.println("Charizard hyökkäsi");
    }

    @Override
    String getState() {
        return "Charizard";
    }

    @Override
    boolean checkEvolve(int xp) {
        return false;
    }

    @Override
    aCharmander evolve() {
        return null;
    }

    @Override
    int getXpToEvolve() {
        return 0;
    }


}
