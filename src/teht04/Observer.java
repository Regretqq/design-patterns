package teht04;

public interface Observer {
    void update(Subject theChangedSubject);
}
