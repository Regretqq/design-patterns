package teht05;

import teht04.Subject;

public class ClockTimer extends Subject {

    private static ClockTimer instance = null;
    private int hour = 0;
    private int minute = 0;
    private int second = 0;

    private ClockTimer(){}

    public static ClockTimer getClockTimer(){
        if (instance == null){
            instance = new ClockTimer();
        }
        return instance;
    }


    public int getHour() {return hour;}

    public int getMinute() {return minute;}

    public int getSecond() {return second;}



    public void tick() {
        if(++second == 60){
            second = 0;
            if(++minute == 60){
                minute = 0;
                hour++;
            }
        }
        notifyObserver();
    }
}

